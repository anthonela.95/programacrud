package com.example.programacrud.model;

public class Usuario {
    private String Id;
    private String Nombre;
    private String Correo;
    private String Contraseña;

    public Usuario() {
    }

  public String getId() {
    return Id;
  }

  public void setId(String id) {
    Id = id;
  }

  public String getNombre() {
    return Nombre;
  }

  public void setNombre(String nombre) {
    Nombre = nombre;
  }

  public String getCorreo() {
    return Correo;
  }

  public void setCorreo(String correo) {
    Correo = correo;
  }

  public String getContraseña() {
    return Contraseña;
  }

  public void setContraseña(String contraseña) {
    Contraseña = contraseña;
  }

  @Override
    public String toString() {
        return Nombre;
    }
}

