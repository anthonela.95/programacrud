package com.example.programacrud;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;
import android.widget.EditText;
import android.widget.ListView;

import com.example.programacrud.model.Usuario;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {
    private List<Usuario> listausuario = new ArrayList<Usuario>();
   ArrayAdapter<Usuario>arrayAdapter;
    EditText nomP,correoP,contraseñaP;
    ListView lisV_usuario;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference DatabaseReference;

    Usuario selecionUsuario;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        nomP= findViewById(R.id.edit_nombre);
        correoP= findViewById(R.id.edit_correo);
        contraseñaP= findViewById(R.id.edit_contrasena);

        lisV_usuario= findViewById(R.id.lv_datosUsuario);
        inicialFirebase();
        listaDatos();

        lisV_usuario.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
             selecionUsuario=(Usuario) parent.getItemAtPosition(position);
             nomP.setText(selecionUsuario.getNombre());
             correoP.setText(selecionUsuario.getCorreo());
             contraseñaP.setText(selecionUsuario.getContraseña());
            }
        });

    }

    private void listaDatos() {
        DatabaseReference.child("Usuario").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                listausuario.clear();
                for(DataSnapshot objSnaptshot: snapshot.getChildren()){
                    Usuario p = objSnaptshot.getValue(Usuario.class);
                    listausuario.add(p);

                    arrayAdapter = new ArrayAdapter<Usuario>(MainActivity.this, android.R.layout.simple_list_item_1, listausuario );
                    lisV_usuario.setAdapter(arrayAdapter);

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void inicialFirebase() {
        FirebaseApp.initializeApp(this);
        firebaseDatabase =firebaseDatabase.getInstance();
      //  firebaseDatabase.setPersistenceEnabled(true); // persistencia
        DatabaseReference = firebaseDatabase.getReference();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected( MenuItem item) {
        String nombre= nomP.getText().toString();
        String correo = correoP.getText().toString();
        String contraseña = contraseñaP.getText().toString();
        switch (item.getItemId()){
            case R.id.icon_add: {
                if(nombre.equals("")||correo.equals("")||contraseña.equals("")){
                    validacion();
                }else {
                    Usuario p= new Usuario();
                    p.setId(UUID.randomUUID().toString());
                    p.setNombre(nombre);
                    p.setCorreo(correo);
                    p.setContraseña(contraseña);
                    DatabaseReference.child("Usuario").child(p.getId()).setValue(p);
                    Toast.makeText(this, "agregado", Toast.LENGTH_SHORT).show();
                    limpiar();


                }
                break;
                }
            case R.id.icon_save: {
                Usuario p = new Usuario();
                p.setId(selecionUsuario.getId());
                p.setNombre(nomP.getText().toString().trim());
                p.setCorreo(correoP.getText().toString().trim());
                p.setContraseña(contraseñaP.getText().toString().trim());
                DatabaseReference.child("Usuario").child(p.getId()).setValue(p);
                Toast.makeText(this, "guardado", Toast.LENGTH_SHORT).show();
                limpiar();
                break;
            }
            case R.id.icon_delete: {
                Usuario p = new Usuario();
                p.setId(selecionUsuario.getId());
                DatabaseReference.child("Usuario").child(p.getId()).removeValue();


                Toast.makeText(this, "eliminado", Toast.LENGTH_SHORT).show();
                break;
            }
            default:break;
        }
        return true;
    }

    private void limpiar() {
        nomP.setText("");
      correoP.setText("");
         contraseñaP.setText("");
    }

    private void validacion() {
        String nombre = nomP.getText().toString() ;
        String correo = correoP.getText().toString();
        String contraseña = contraseñaP.getText().toString();
        if (nombre.equals("")){
            nomP.setError("Required");

        }else if (correo.equals("")){
            correoP.setError("Required");
        }else if (contraseña.equals("")){
            contraseñaP.setError("Required");

        }

    }
}
